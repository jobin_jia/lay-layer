import {Area, LayerProps, LayerType, Offset} from "./layerProps";
import {isArray, isNumber} from "./is";

export function getDrawerAnimationClass(offset: Offset, isClose: boolean = false) {
  const prefix = "layer-drawer-anim layer-anim";
  let suffix = "rl";
  if (offset === "l") {
    suffix = "lr";
  } else if (offset === "r") {
    suffix = "rl";
  } else if (offset === "t") {
    suffix = "tb";
  } else if (offset === "b") {
    suffix = "bt";
  }
  return isClose ? `${prefix}-${suffix}-close` : `${prefix}-${suffix}`;
}


/**
 * get dom offset
 * @param el
 * @param globalConfig
 * @param offset
 * @param area
 * @param type
 */
export function calcOffset(el: HTMLElement, globalConfig: LayerProps, offset: Offset, area: Area, type: LayerType): [number, number] {
  const domEl = Reflect.get(el, '$el')
  const [domWidth, domHeight] = [domEl.offsetWidth, domEl.offsetHeight]
  // 可视windows区域
  const windowViewWidth = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
  const windowViewHeight = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)

  let offsetTop = (windowViewHeight - domHeight) / 2
  let offsetLeft = (windowViewWidth - domWidth) / 2

  if (globalConfig && globalConfig.offset) {
    const { offset } = globalConfig
    // 单个为只定义top
    if (isNumber(offset)) {
      offsetTop = offset
    } else if (isArray<number>(offset)) {
      const [t, l] = offset
      offsetTop = t
      offsetLeft = l
    } else if (offset !== 'auto') {
      switch (offset) {
        case 't':
          offsetTop = 0
          break
        case 'r':
          offsetLeft = windowViewWidth - domWidth
          break
        case 'b':
          offsetTop = windowViewHeight - domHeight
          break
        case 'l':
          offsetLeft = 0
          break
        case 'lt':
          offsetLeft = 0
          offsetTop = 0
          break
        case 'lb':
          offsetTop = windowViewHeight - domHeight
          offsetLeft = 0
          break
        case 'rt':
          offsetTop = 0
          offsetLeft = windowViewWidth - domWidth
          break
        case 'rb':
          offsetTop = windowViewHeight - domHeight
          offsetLeft = windowViewWidth - domWidth
          break
        default:
          break
      }
    }
  }

  console.log(offsetLeft, offsetTop)

  // todo: if !config.fixed

  return [offsetTop, offsetLeft]
}
