import {LayerType} from "./layerProps";

export function getSlots(type: LayerType, slots: Record<string, any>): string[] {
  let defSlots = ['default']

  if (type === 0) {
    defSlots = [...defSlots, 'title']
  }

  return defSlots
}
