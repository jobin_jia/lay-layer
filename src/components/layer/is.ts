
export function isNumber (val: any): val is number {
  return Object.prototype.toString.call(val) === '[object Number]'
}

export function isArray<T> (val: any): val is Array<T> {
  return Object.prototype.toString.call(val) === '[object Array]'
}
