import { Slot } from "vue";

export interface LayerSlots {
  default: () => Slot // content
  title: () => string
  icon: () => Slot
  btn: () => Slot
}