import {LayerProps} from "../layerProps";

export class Layer {
  private config: LayerProps = {} as LayerProps
  public win: Record<string, any> = {}
  constructor(config: LayerProps) {
    this.config = config
    this.initWindowConfig()
  }

  initWindowConfig() {
    this.win = {
      innerWidth: window.innerWidth,
      innerHeight: window.innerHeight,
    }
  }

  open () {}
  confirm () {}
  close () {}
  closeAll () {}
}
