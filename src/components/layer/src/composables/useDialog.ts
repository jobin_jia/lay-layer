import {LayerProps} from "../../layerProps";
import {computed, ExtractPropTypes, Ref, toRefs, unref} from "vue";
import {calcOffset} from "../../utils";

export function useDialog(props: Readonly<ExtractPropTypes<LayerProps>>, layero: Ref<HTMLElement | null>) {
  const {
    type,
    offset,
    area,
    anim,
    isOutAnim,
    visible
  } = toRefs(props)

  const dialogStyle = computed(() => {
    if (unref(layero)) {
      const [ top, left ] = calcOffset(unref(layero) as HTMLElement, {} as LayerProps /* 全局配置 */, unref(offset)!, unref(area)!, unref(type)!)
      return {
        top: `${top}px`,
        left: `${left}px`
      }
    }
    return {}
  })

  return {
    dialogStyle
  }
}
