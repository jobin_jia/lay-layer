import { createApp } from 'vue'
import App from './App.vue'
import './components/layer/src/layer.css'
createApp(App).mount('#app')
